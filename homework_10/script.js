'use strict';
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
const email=document.querySelector('.enter__table-email-input');
const starE=document.querySelector('.enter__table-email-title-star')
const nameE=document.querySelector('.enter__table-email-title')
const spanE=document.querySelector('.spanEmail')

function emailCheck(){
    if(email.value===''){
  email.style.border = '2px solid #CB2424';
  starE.style.color='#CB2424';
  nameE.style.color='#CB2424';
  spanE.textContent='Поле обязательно для запонения'
    }
    if (validateEmail(email.value)===false){
        email.style.border = '2px solid #CB2424';
        starE.style.color='#CB2424';
        nameE.style.color='#CB2424';
        spanE.textContent='Email невалидный'
    }
    else console.log('Почта  ' + email.value)

return email.style
}
const password=document.querySelector('.enter__table-password-input');
const starP=document.querySelector('.enter__table-password-title-star')
const nameP=document.querySelector('.enter__table-password-title')
const spanP=document.querySelector('.spanPassword')
//вызываем функцию
function passwordCheck(){
    if(password.value){
        password.style.border = '2px solid #787878';
    }
    if(password.value===''){
        password.style.border = '2px solid #CB2424';
        starP.style.color='#CB2424';
        nameP.style.color='#CB2424';
        spanP.textContent='Поле обязательно для запонения'
    }
    if(password.value.length<8){
        password.style.border = '2px solid #CB2424';
        starP.style.color='#CB2424';
        nameP.style.color='#CB2424';
        spanP.textContent='Пароль должен содержать как минимум 8 символов'
    }
    else{
        console.log('Пароль  ' + password.value)
    }
    return password.style
}
const checkBox=document.querySelector('.enter__table-checkbox-input');
const spanC=document.querySelector('.spanCheck')
const starC=document.querySelector('.enter__table-checkbox-star');
const markC=document.querySelector('.enter__table-checkbox-mark' );
function checkBoxCheck(){

    if(!checkBox.checked){
        markC.style.borderColor='#CB2424';
        starC.style.color='#CB2424';
        spanC.textContent='Поле обязательно для запонения'
    }
    return checkBox
}


const submit=document.querySelector('.submit__button');
//добавляем событие браузера, клик по кнопке сабмит
submit.addEventListener('click',(event)=>{
    //при клике пос сабмиту должны выполняться следующие условия

    console.log('Кнопка клик 1');
    emailCheck();
    passwordCheck();
    checkBoxCheck();
    event.preventDefault();

})

import React, {useState} from 'react'
import styles from './AuthPage.module.scss'
import {observer} from "mobx-react-lite";
import AuthState from "../../components/Market/AuthState";
import {setRegister, getRegister, delUser} from "../../components/Services/LSRegister";
import {setAuth} from "../../components/Services/LSAuth";
import {DtfLogo} from "../../components/icons/DtfLogo";

export const AuthPage = observer(() => {
    const [form, setForm] = useState({login: '', password: ''})

    const changeForm = (e) => {
        setForm({...form, [e.currentTarget.name]: e.currentTarget.value})
    }

    const registerHandler = () => {
        setRegister(form.login, form.password)
    }

    const loginHandler = () => {
        const users = JSON.parse(getRegister())
        if (users) {
            const candidate = users.filter(i => i.login === form.login)
            if (candidate.length !== 0) {
                if (candidate[0].password === form.password) {
                    setAuth()
                    return AuthState.setAuth()
                }
                return alert('Пароль не верный')
            }

        }
        return alert('Такого пользователя нет')
    }

    const delUserHandler = () => {
        delUser(form.login)
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.Auth_Image}> <div className={styles.LogoOnImage}><DtfLogo/></div>
                <img src="https://leonardo.osnova.io/356f8b87-e803-86ab-2875-f11915f78a7e/-/format/webp/" alt=""/>
            </div>
            <div className={styles.form}>
              <div className={styles.InputLogin}>  <input name={'login'} placeholder={'логин'} value={form.login} onChange={changeForm}/>
            </div>
                <div className={styles.InputPassword}><input name={'password'} placeholder={'пароль'} value={form.password} onChange={changeForm}/>
            </div>
                <div className={styles.buttons}>
                    <button onClick={loginHandler}>Войти</button>
                    <button onClick={registerHandler}>Регистрация</button>
                    <button onClick={delUserHandler}>Удалить</button>
                </div>
            </div>
        </div>
    )
})

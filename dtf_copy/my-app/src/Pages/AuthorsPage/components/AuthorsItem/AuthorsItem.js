import React, {useEffect} from "react";
import {useParams} from "react-router-dom";
import {observer} from "mobx-react-lite";
import UserState from "../../../../components/Market/UserState";
import styles from "../../../../components/Authors/Authors.module.scss";

export const AuthorsItem = observer(() => {
    const {id} = useParams()

    useEffect(() => {
        if (UserState.users.length === 0) {
            UserState.requestUsers()
        }
    }, [])

    const data = UserState.users.filter(c => c.id === Number(id))[0]

    return (
        <div className={styles.wrapper}>
        <div className={styles.Avatar}> <img src={data?.avatar} alt={id}/></div>
        <div className={styles.data}>
            <div className={styles.Fname}>
                First name:
                {data?.fName}</div>
            <div className={styles.Lname}>
                Last name:
                {data?.lName}</div>
            <div className={styles.Email}>Email:
                {data?.email}</div>

        </div>
        </div>
    )
}
            )


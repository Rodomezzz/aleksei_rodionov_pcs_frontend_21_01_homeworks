import React, {useState, useEffect} from "react";
import styles from './AuthorsPage.module.scss'
import axios from 'axios'
import {User} from "../../components/Authors/Authors";
import {observer} from "mobx-react-lite";
import UserState from "../../components/Market/UserState";

export const AuthorsPage = observer(() => {
    useEffect(() => {
        UserState.requestUsers()
    }, [])

    const users = UserState.users

    const items = users.map(c => <User id={c.id} fName={c.first_name} lName={c.last_name} email={c.email} avatar={c.avatar}/>)

    return (
        <div className={styles.wrapper}>


            <div className={styles.list}>
                {users.length !== 0 ? items : 'Список пуст'}
            </div>

        </div>
    )
})
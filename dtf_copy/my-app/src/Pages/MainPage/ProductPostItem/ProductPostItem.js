import React, {useEffect} from "react";
import {useParams} from "react-router-dom";
import {observer} from "mobx-react-lite";
import PostState from "../../../components/Market/PostState";
import styles from "../../../components/Posts/Posts.module.css";



export const ProductPostItem=observer(()=>{
    const {id}=useParams()

    useEffect(()=>{
        if(PostState.posts.length === 0){
            PostState.requestPosts()
        }
    },[])

    const data=PostState.posts.filter(i=>i.id===Number(id))[0]

    console.log(data)
    return(
        <div className={styles.ContentContainer}>
            <div className={styles.title}>
                {data?.title}
                <div className={styles.Intro}>{data?.intro}</div>
                <div className={styles.Cover}>
                    <img src={data?.cover && data?.cover.url} />
                </div>
                <div className={styles.summarize}>   {data?.summarize}</div>
            </div>

        </div>
    )
})
import React, {useState, useEffect} from "react";
import axios from 'axios';
import {Post} from "../../components/Posts/Posts";
import {observer} from "mobx-react-lite";
import PostState from "../../components/Market/PostState";




export const MainPage= observer(() =>{
    useEffect(()=>{
    PostState.requestPosts()
    }, [])
    const posts=PostState.posts
    const items=posts.map(i=> <Post id={i.id}  title={i.title} summ={i.summarize} url={i.url} intro={i.intro} cover={i.cover && i.cover.url}/>)
    return(
        <div >

            {posts.length!==0 ? items : 'Постов нет'}
        </div>
    )

})
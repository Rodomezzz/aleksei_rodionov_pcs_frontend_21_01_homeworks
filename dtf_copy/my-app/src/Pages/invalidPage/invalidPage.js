import React from "react";
import {InvalidLogo} from "../../components/icons/InvalidIcon";
import styles from './invalidPage.module.css'

export const InvalidPage= () =>{
    return(
<div className={styles.headTitle}>
    <div className={styles.errorLogo}><InvalidLogo/></div>
     <div className={styles.title}>Ошибка 404
         <div className={styles.mainText}>К сожалению, запрашиваемая страница не найдена, но есть другие, тоже хорошие</div>
     </div>

</div>
    )

}
import React, {useState, useEffect} from "react";
import {Header} from "../Header/Header";
import {Body} from "../Body/Body";
import {routeService} from "../Services/routeService";
import styles from './App.module.css'
import AuthState from "../Market/AuthState";
import {observer} from "mobx-react-lite";
import ScrollToTopNoButton from "../Services/ScrollToTopNoButton";
import ScrollToTop from "react-scroll-to-top";
import {getAuth} from "../Services/LSAuth";
// import {AuthPage} from "../../Pages/AuthPage/AuthPage";


export const App =observer(() =>{
    const isAuth=AuthState.isAuth
    const router=routeService(isAuth)

    useEffect(() => {
        const checkAuth = getAuth()
        if (checkAuth) {
            AuthState.setAuth()
        }
    }, [])
    // const isAuthLogin=AuthState.isAuth(form.login)

    return (
        <div>

            <div className={styles.headHeader}>  <Header />
                <div className={styles.headAuth}>{isAuth ? <span className={styles.AuthTrue}>Авторизован</span> : ''}</div>
            </div>
            <div className={styles.bodyBody}>
                <Body/> <div className={styles.bodyContent}> <ScrollToTopNoButton/> {router}</div>
            </div>
<ScrollToTop/>
        </div>
    )
})
import React from "react";
import styles from './Body.module.css'
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react-lite";
import AuthState from "../Market/AuthState";
import {Popular} from "../icons/Popular";


export const Body =observer(() =>{

    const isSidebar = AuthState.isSidebar

    const LeftMenuList = [
        {icon:{Popular},id: 1, name: 'Популярное', link:'/'},
        {id: 2, name:  'Свежее', link:'#'},
        {id: 3, name: 'Авторы DTF', link:'/authors'},
        {id: 4, name: 'Рейтинги DTF', link:'/ratio'},
        {id: 5, name: 'Аккаунт', link:'/user'},
    ];
    const MenuItems=LeftMenuList.map(i => <NavLink icon={i.icon} to={i.link} key={i.id}>{i.name}</NavLink>);
    return(
        <div className={styles.Body}>
            {isSidebar &&(
                <div className={styles.Body__Sidebar}>{MenuItems}</div>
            )}

            <div className={styles.Body__news}></div>
            {/*<span className={styles.Body__spacerText}>Здесь могла быть ваша реклама</span>*/}
        </div>
    )
})

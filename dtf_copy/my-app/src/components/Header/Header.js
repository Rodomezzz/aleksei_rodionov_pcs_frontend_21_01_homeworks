import React from "react";
import styles from './Header.module.css';
import {SearchIcon} from "../icons/SearchIcon";
import {DtfLogo} from "../icons/DtfLogo";
import {MenuIcon} from "../icons/MenuIcon";
import {Notifications} from "../icons/Notifications";
import {UserLogin} from "../icons/UserLogin";
import {UserLoginLogin} from "../icons/UserLoginLogin";
import {ArrowDown} from "../icons/ArrowDown";
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react-lite";
import AuthState from "../Market/AuthState";


export const Header = observer(() => {
    const isAuth=AuthState.isAuth
    const toggleSideBar=()=>{AuthState.toggleSideBar()}
    // const IsOpen=()=>{AuthState.isSidebar}
    const toggleAuth=()=>{
        if(isAuth){
            return AuthState.noAuth()
        }
        return AuthState.setAuth()
    }
    return(
        <div className={styles.header}>
            <div className={styles.header__menu}>

                <button className={styles.header__menu_button} onClick={toggleSideBar}> <MenuIcon/></button>   </div>
            <div className={styles.header__logo}>
                <button className={styles.header__logo_button}> <NavLink to={'/'}> <DtfLogo/></NavLink> </button>

            </div>
            <div className={styles.header__findbar}>
                <SearchIcon/>
                <input type="text" placeholder="Поиск"/>
            </div>
            <div className={styles.header__newpost}>
                <button className={styles.header__newpost_newpost}> Новая запись </button>
                <button className={styles.header__newpost_arrow}> <ArrowDown/></button>
            </div>
            <div className={styles.spacer}> </div>
            <div className={styles.header__noitification}>
                <button className={styles.button_Notification}><Notifications/></button>
            </div>
            <div className={styles.header__user_login}>{isAuth ?  <button className={styles.button_UserLoginLogin} onClick={toggleAuth}><UserLoginLogin/></button> :  <button className={styles.button_UserLogin} onClick={toggleAuth}>
                <UserLogin/><span className={styles.header__user_login_text}>
                    Войти</span></button>}


            </div>


        </div>
    )
})
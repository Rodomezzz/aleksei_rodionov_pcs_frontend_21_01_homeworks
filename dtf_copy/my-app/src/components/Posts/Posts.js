import React from "react";
import styles from './Posts.module.css'
import {NavLink} from "react-router-dom";


export const Post=({id, title,  summ, url, intro, cover})=>{
    return(
      <NavLink className={styles.NavLink} to={`/post/${id}`}>
          <div className={styles.ContentContainer}>
            <div className={styles.title}>
            {title}
                <div className={styles.Intro}>{intro}</div>
                <div className={styles.Cover}>
                    <img src={cover} alt={id}/>
                </div>
            <div className={styles.summarize}>   {summ}</div>
        </div>

            </div>
</NavLink>
)

}
const lsName = 'auth_dtf'

export const getAuth = () => {
    return localStorage.getItem(lsName)
}

export const setAuth = () => {
    localStorage.setItem(lsName, 'true')
}

export const delAuth = () => {
    localStorage.removeItem(lsName)
}
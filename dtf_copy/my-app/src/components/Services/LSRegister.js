const lsName = 'reg_dtf'

export const getRegister = () => {
    return localStorage.getItem(lsName)
}

export const setRegister = (login, password) => {
    const oldLS = getRegister()
    if (oldLS === null) {
        return localStorage.setItem(lsName,JSON.stringify([{login, password}]))
    }
    let array = JSON.parse(oldLS)
    const newArr = [...array, {login, password}]
    // array.push({login, password})

    localStorage.setItem(lsName, JSON.stringify(newArr))
}

export const delUser = (login) => {
    const oldLS = getRegister()
    if (oldLS) {
        const arr = JSON.parse(oldLS)
        const newArr = arr.filter(i => i.login !== login)
        localStorage.setItem(lsName, JSON.stringify(newArr))
    }
}
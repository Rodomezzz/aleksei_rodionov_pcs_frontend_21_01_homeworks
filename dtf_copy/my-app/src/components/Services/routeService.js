import {Routes,Route,Navigate} from 'react-router-dom'
import {MainPage} from "../../Pages/MainPage/MainPage";
import {Ratio} from "../../Pages/Ratio/Ratio";
import {InvalidPage} from "../../Pages/invalidPage/invalidPage";
import {UserPage} from "../../Pages/userPage/userPage";
import {ProductPostItem} from "../../Pages/MainPage/ProductPostItem/ProductPostItem";
import {AuthorsItem} from "../../Pages/AuthorsPage/components/AuthorsItem/AuthorsItem";
import React from "react";
import {AuthPage} from "../../Pages/AuthPage/AuthPage";
import {AuthorsPage} from "../../Pages/AuthorsPage/AuthorsPage";
import {RatioX} from "../../Pages/Ratio/Ratio";

export const routeService=(isAuth)=>{
    if(isAuth){
        return (
        <Routes>
       <Route path={'/'} exact element={<MainPage/>}/>
       <Route path={'/post/:id'}  element={<ProductPostItem/>}/>
       <Route path={'/ratio'} element={<RatioX/>}/>
       <Route path={'/authors'} exact element={<AuthorsPage/>}/>
       <Route path={'/authors/:id'} element={<AuthorsItem/>}/>
       <Route path={'/user'} element={<UserPage/>}/>
       <Route path={'*'} element={<InvalidPage/>}/>
   </Routes>)
   }
   return( <Routes>
       {/*<Route path={'/'} element={<MainPage/>}/>*/}
       {/*<Route path={'/productpage'} element={<ProductPage/>}/>*/}
       {/*<Route path={'/favorites'} element={<Ratio/>}/>*/}
       <Route path={'/'} element={<AuthPage/>}/>
       <Route path={'*'} element={<Navigate to={'/'}/>}/>

   </Routes>)

}



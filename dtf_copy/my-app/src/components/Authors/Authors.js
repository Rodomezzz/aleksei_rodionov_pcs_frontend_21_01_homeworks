import React from "react";
import styles from './Authors.module.scss'
import {NavLink} from "react-router-dom";
import {observer} from "mobx-react-lite";


export const User = observer(({id, fName, lName, email, avatar}) => {


    return (
        <NavLink className={styles.Navigate} to={`/authors/${id}`}>
            <div className={styles.wrapper}>
                <div className={styles.Avatar}><img src={avatar} alt={id}/></div>
                <div className={styles.data}>

                    <div className={styles.Fname}>
                        First name:
                        {fName}</div>
                    <div className={styles.Lname}>
                        Last name:
                        {lName}</div>
                    <div className={styles.Email}>
                        Email:
                        {email}</div>

                </div>

            </div>
        </NavLink>
    )
})
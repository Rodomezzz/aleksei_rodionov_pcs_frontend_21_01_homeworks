import {makeAutoObservable} from "mobx"
import axios from "axios";

class UsersState {
    constructor() {
        makeAutoObservable(this)
    }

    users = []

    requestUsers = () => {
        axios.get('https://reqres.in/api/users?per_page=100')
            .then(res => {
                this.users = res.data.data
            })
            .catch(err => {
                console.log(err)
            })
    }

    deleteUser = (id) => {
        this.users = this.users.filter(c => c.id !== id)
    }

}

export default new UsersState()
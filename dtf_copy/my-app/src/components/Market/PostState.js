import {makeAutoObservable} from "mobx";
import axios from "axios";


class UserState{
    constructor() {
        makeAutoObservable(this)
    }
    posts=[]

    requestPosts=()=>{
        axios.get( "https://api.dtf.ru/v1.9/news/default/recent?ref=dtf.ru")
            .then(res=>{
                this.posts=res.data.result

                console.log(res.data.result)
            })
            .catch(err=>{
                console.log(err)
            })
    }
}

export default new UserState()
import {makeAutoObservable} from "mobx";
// import {delAuth} from "../services/LSAuth";

class AuthState {
    constructor() {
        makeAutoObservable(this)
    }
    
    isSidebar = false

    toggleSideBar = () => {
        this.isSidebar = !this.isSidebar
    }


    isAuth = false
    setAuth = () => {
        this.isAuth = true
    }

    noAuth = () => {
        // delAuth()
        this.isAuth = false
    }

}

export default new AuthState()
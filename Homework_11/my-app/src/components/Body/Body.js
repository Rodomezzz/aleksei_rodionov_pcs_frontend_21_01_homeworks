import React from "react";
import styles from './Body.module.css'
export const Body = () =>{
    const LeftMenuList = [
        {id: 1, name: 'Популярное', link:'#'},
        {id: 2, name:  'Свежее', link:'#'},
        {id: 3, name: 'Вакансии', link:'#'},
        {id: 4, name: 'Рейтинги DTF', link:'#'},
        {id: 5, name: 'Подписки', link:'#'},
    ];
    const MenuItems=LeftMenuList.map(i => <a key={i.id} href={i.link}>{i.name}</a>);
    return(
        <div className={styles.Body}>
          <div className={styles.Body__Sidebar}>{MenuItems}</div>
        <div className={styles.Body__news}>
            Ворованные новости
        </div>
        <div className={styles.Body__spacer}>
            {/*<span className={styles.Body__spacerText}>Здесь могла быть ваша реклама</span>*/}
        </div>
        </div>
    )
}

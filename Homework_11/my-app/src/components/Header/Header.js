import React from "react";
import styles from './Header.module.css';
import {SearchIcon} from "../icons/SearchIcon";
import {DtfLogo} from "../icons/DtfLogo";
import {MenuIcon} from "../icons/MenuIcon";
import {Notifications} from "../icons/Notifications";
import {UserLogin} from "../icons/UserLogin";
import {ArrowDown} from "../icons/ArrowDown";


export const Header = () => {

    return(
        <div className={styles.header}>
        <div className={styles.header__menu}>

            <button className={styles.header__menu_button}><MenuIcon/> </button>   </div>
            <div className={styles.header__logo}>
              <DtfLogo/>

        </div>
            <div className={styles.header__findbar}>
                <SearchIcon/>
                <input type="text" placeholder="Поиск"/>
            </div>
            <div className={styles.header__newpost}>
                <button className={styles.header__newpost_newpost}> Новая запись </button>
                <button className={styles.header__newpost_arrow}> <ArrowDown/></button>
            </div>
            <div className={styles.spacer}></div>
            <div className={styles.header__noitification}>
                <button className={styles.button_Notification}><Notifications/></button>
            </div>
            <div className={styles.header__user_login}>
                <button className={styles.button_UserLogin}><UserLogin/>Войти</button>
                {/*<span className={styles.header__user_login_text}>Войти</span>*/}
            </div>


        </div>
    )
};
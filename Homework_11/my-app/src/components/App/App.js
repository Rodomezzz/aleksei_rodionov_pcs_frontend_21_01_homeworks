import React from "react";
import {Header} from "../Header/Header";
import {Body} from "../Body/Body";
import {MainPage} from "../../Pages/MainPage/MainPage";
import {ProductPage} from "../../Pages/ProductPage/ProductPage";
import {routeService} from "../Services/routeService";


export const App =() =>{
    const router=routeService()
    return(
        <div>
           <Header/>
            {router}
           <MainPage/>
            <ProductPage/>
            <Body/>



        </div>
    )
}